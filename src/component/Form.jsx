import React, { Component } from "react";
import Table from "./Table";

export default class Form extends Component {
  constructor() {
    super();
    this.state = {
      form: [
        { id: 1, Email: "rotanakkosal03@gmail.com", UserName:"Rotanakkosal",Age:21,status:"Pending"},
      ],
      Email: " null",
      UserName:"null",
      Age:" null",
      status:"Pending"
    };
  }
  changeStatus= (item)=>{
     item.status = item.status === "Pending"? "Done": "Pending";
     this.setState({
      item:this.state.form
     })
    //  this.setState({
    //    status: "Done",
    //  });
  };

  handleEmail = (event) => {
    this.setState({
      Email: event.target.value,
    });
  };
  handleUserName = (event) => {
    this.setState({
     UserName: event.target.value,
    },()=>console.log(this.state.UserName));
  };
  handleAge = (event) => {
    this.setState({
     Age: event.target.value,
    });
  };
  handleSubmit = () => {
    
    const newObj = { 

      id: this.state.form.length + 1,
      Email: this.state.Email,
      UserName: this.state.UserName,
      Age:this.state.Age,
      status:this.state.status
    };
    // console.log(newObj)
    this.setState(
      {
        form: [...this.state.form, newObj],
        newForm: "",
      },
      () => console.log(this.state.form)
    );
  };
  render() {
    return (
      <div className=" w-screen  center flex flex-col  justify-center items-center pt-24 ">
        <h1 className="text-4xl font-bold text-teal-600 ">Please Fill in your information</h1>
        <label for="email-address-icon" class="block mb-2 text-lg  font-medium text-gray-900 pt-12 ">Your Email</label>
        <div class="flex">
          <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
            <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path><path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path></svg> 
          </span>
          <input onChange={this.handleEmail}   type="text" id="website-admin" class="rounded-none rounded-r-lg bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-96 text-sm p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Ngorn@gmail.com " />
        </div>
        <label for="website-admin" class="block mb-2 text-lg font-medium text-gray-90">Username</label>
        <div class="flex">
          <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
            @
          </span>
          <input onChange={this.handleUserName}  type="text" id="website-admin" class="rounded-none w-96 rounded-r-lg bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0  text-sm p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Seangngorn" />
        </div>

        <label for="email-address-icon" class="block mb-2 text-lg font-medium text-gray-900 ">Age</label>
        <div class="flex">
          <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
            Age
          </span>
          <input onChange={this.handleAge} type="text" id="website-admin" class="rounded-none w-96 rounded-r-lg bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0  text-sm p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="age....."/>
        </div>
        <button
          onClick={this.handleSubmit} 
          class="bg-white-500 border-2 w-40 border-sky-500 mt-4 hover:bg-blue-700 hover:text-white text-black font-bold py-2 px-4 rounded"
        >
          Register
        </button>
        <Table data = {this.state.form}  changeStatus={this.changeStatus} />
      </div>
    );
  }
}
