import React, { Component } from 'react'
import Swal from 'sweetalert2'
import 'animate.css';
export default class Table extends Component {
  showAlert = (item) => {
    Swal.fire({
      showClass: {
        popup: 'animate__animated animate__fadeInDown'
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp'
      },
        title: "ID: "+item.id 
         +"\n Email: "+item.Email 
         +"\n UserName: "+item.UserName 
         +"\n Age: "+item.Age ,
        width: 800,
        padding: '3em',
        color: 'blue',
        borderRadius:'50px',
        background: '#fff url(https://i.pinimg.com/originals/e6/20/83/e62083244f28ed2024a09ac52db6bceb.gif)',
        backdrop: `
        rgb(192,192,192)
          url("https://media.tenor.com/4VLUJVRqolcAAAAM/verivery-animated.gif")
          left top
          no-repeat    
        `
     })
}
  render() {
      // var style={
      //   tr:{nth_Child(even){
      //       backgroundColor:red
      //   }}
      // }
    return (
      <div>
        <div class="relative overflow-x-auto shadow-md shadow-orange-900 sm:rounded-lg mt-24 ml-36">
          <h2 className='font-bold text-2xl mb-5'>Output Student Name</h2>
          <table class="w-full text-sm text-left text-black-500 dark:text-black-400">
            <thead class="text-xs center text-black-100 items-center uppercase bg-red-50 dark:bg-red-100 dark:text-black-400">
              <tr>
                <th scope="col" class="px-6 py-3">
                  ID STUDENT
                </th>
                <th scope="col" class="px-6 py-3">
                  EMAIL
                </th>
                <th scope="col" class="px-6 py-3">
                  USERNAME
                </th>
                <th scope="col" class="px-6 py-3">
                  Ages
                </th>
                <th scope="col" class="px-6 py-3">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              {this.props.data.map((item) => (
                <tr  key={item.id} class=" border-b text-xl w-1/2 center dark:bg-reg-400 dark:border-red-400">
                  <td class="px-6 py-4">{item.id}</td>
                  <td class="px-6 py-4">{item.Email}</td>
                  <td class="px-6 py-4">{item.UserName}</td>
                  <td class="px-6 py-4">{item.Age}</td>
                  <td class="px-6 py-4">
                  <button onClick={()=>this.props.changeStatus(item)} class={` text-xs text-white font-bold py-2 px-5 mr-2 rounded-xl ${item.status==="Pending" ? `bg-red-500`:`bg-cyan-400`}`}>
                    {item.status}
                  </button>
                  <button onClick={()=> this.showAlert(item)}  class="bg-blue-700 text-xs hover:bg-blue-700 text-white font-bold py-2 px-5 rounded-xl" >
                    Show more
                  </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}
